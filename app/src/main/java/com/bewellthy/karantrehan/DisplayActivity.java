package com.bewellthy.karantrehan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bewellthy.karantrehan.adapters.DbAdapter;
import com.bewellthy.karantrehan.adapters.WordAdapter;
import com.bewellthy.karantrehan.models.Word;
import com.bewellthy.karantrehan.utils.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DisplayActivity extends AppCompatActivity {

    @Bind(R.id.rvWords)
    RecyclerView rvWords;
    @Bind(R.id.fabRefresh)
    FloatingActionButton fabRefresh;
    @Bind(R.id.tvWentWrong)
    TextView tvWentWrong;
    Context mCon;

    //Database
    private static DbAdapter mDbHelper;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    //DownloadData
    String getWordsUrl;
    private OkHttpClient client;
    ProgressDialog progress;
    String TAG = "Display";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        ButterKnife.bind(this);
        mCon = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(mCon);
        editor = prefs.edit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDbHelper = new DbAdapter(mCon);
        try {
            mDbHelper.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (prefs.getBoolean("FirstLaunch", true)) {
            getWords();
        } else {
            setMyRecyclerView();
        }
        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWords();
            }
        });

    }


    public void setMyRecyclerView() {
        rvWords.setLayoutManager(new LinearLayoutManager(mCon));
        List<Word> result = new ArrayList<>();

        Cursor wordCur = mDbHelper.fetchWordsDis();
        if (wordCur != null && wordCur.getCount() > 0) {
            while (wordCur.moveToNext()) {
                int index0 = wordCur.getColumnIndex(mDbHelper.KEY_W_ID);
                int index1 = wordCur.getColumnIndex(mDbHelper.KEY_W_WORD);
                int index2 = wordCur.getColumnIndex(mDbHelper.KEY_W_VARIANT);
                int index3 = wordCur.getColumnIndex(mDbHelper.KEY_W_MEANING);
                int index4 = wordCur.getColumnIndex(mDbHelper.KEY_W_RATIO);

                String id = wordCur.getString(index0);
                String word = wordCur.getString(index1);
                String variant = wordCur.getString(index2);
                String meaning = wordCur.getString(index3);
                String ratio = wordCur.getString(index4);

                //new RecScheduleModel(teamOne,teamTwo,venue,date,time,win);

                result.add(new Word(id, word, variant, meaning, ratio));
            }

            WordAdapter sa = new WordAdapter(mCon, result);
            rvWords.setAdapter(sa);
        }
    }

    private void getWords() {
        CheckNetwork cn = new CheckNetwork(mCon);
        if (cn.isConnectingToInternet()) {
            getWordsUrl = mCon.getResources().getString(R.string.wordsUrl);
            getWordsAsync();
        } else {
            cn.showAlert();
        }
    }


    private void getWordsAsync() {
        progress = new ProgressDialog(mCon);
        progress.setMessage(Html
                .fromHtml("<b>Loading words</b><br/>Please Wait..."));
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        tvWentWrong.setVisibility(View.GONE);

        //Cache
        File cacheDir = null;
        final File baseDir = mCon.getCacheDir();
        if (baseDir != null) {
            cacheDir = new File(baseDir, "HttpResponseCache");
        }
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(cacheDir, cacheSize);


        client = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(getWordsUrl)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(mCon, "Failed to get words. Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d(TAG, "run: " + e.getMessage());
                        tvWentWrong.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String serresponse = response.body().string();
                //Log.d(TAG, "onResponse: " + serresponse);

                JSONArray jsonArray = null;
                mDbHelper.trunFix();
                String displ = "1";

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(serresponse);
                    jsonArray = jsonObject.getJSONArray("words");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject curWord = jsonArray.getJSONObject(i);
                        String id = curWord.getString("id");
                        String word = curWord.getString("word");
                        String variant = curWord.getString("variant");
                        String meaning = curWord.getString("meaning");
                        String ratio = curWord.getString("ratio");

                        if (ratio.contains("-")) {
                            displ = "0";
                        } else {
                            displ = "1";
                        }
                        mDbHelper.insertWords(id, capitalize(word), variant, meaning, ratio, displ);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSonExc " + e.getMessage());
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        editor.putBoolean("FirstLaunch", false).commit();
                        progress.dismiss();
                        setMyRecyclerView();
                    }
                });

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.me) {
            String url = "http://www.linkedin.com/in/trehankaran";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (id == android.R.id.home) {
            finish();
        }
        return true;
    }

    private String capitalize(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
                found = false;
            }
        }
        return String.valueOf(chars);
    }
}
