package com.bewellthy.karantrehan.utils;

/**
 * Created by KaranTrehan on 04-06-2015.
 */

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.Html;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bewellthy.karantrehan.R;

public class CheckNetwork {

    private Context mCon;

    public CheckNetwork(Context context) {
        this.mCon = context;
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) mCon.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public void showAlert() {

        new MaterialDialog.Builder(mCon)
                .title(":(")
                .content(Html.fromHtml("We require an active connection to the internet to be able to " +
                        "fetch amazing words for you and other operations. " +
                        "<b>Kindly get connected to the internet.</b><br/>We recommend Wi-Fi!"))
                .positiveText("WiFi :)")
                .negativeText("Settings")
                .titleColorRes(R.color.primary_dark)
                .positiveColorRes(R.color.accent)
                .negativeColorRes(R.color.accent)
                .contentColorRes(R.color.primary)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        mCon.startActivity(intent);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        mCon.startActivity(intent);
                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                    }
                })
                .cancelable(false)
                .show();

    }
}
