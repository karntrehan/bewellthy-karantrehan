package com.bewellthy.karantrehan.models;

/**
 * Created by karan_000 on 04-03-2016.
 */
public class Word {

    public  String id;
    public  String word;
    public  String variant;
    public  String meaning;
    public  String ratio;

    public Word(String id, String word, String variant, String meaning, String ratio) {
        this.id = id;
        this.word = word;
        this.variant = variant;
        this.meaning = meaning;
        this.ratio = ratio;
    }
}
