package com.bewellthy.karantrehan.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;

public class DbAdapter {


    //Fixture
    public static final String KEY_W_ID = "id";
    public static final String KEY_W_WORD = "word";
    public static final String KEY_W_VARIANT = "variant";
    public static final String KEY_W_MEANING = "meaning";
    public static final String KEY_W_RATIO= "ratio";
    public static final String KEY_W_DISPLAY= "display";


    private static final String DATABASE_NAME = "words_db";
    private static final String WORDS_TABLE = "words";
    private static final int DATABASE_VERSION = 1;

    //Create FIXTURE
    private static final String DATABASE_FIXTURE =
            "create table words (id text, "
                    + "word text, variant text, meaning text,ratio text, display text);";

    private final Context mCtx;
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;


    public DbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public DbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public long insertWords(String id, String word, String variant, String meaning, String ratio, String display) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_W_ID, id);
        cv.put(KEY_W_WORD, word);
        cv.put(KEY_W_VARIANT, variant);
        cv.put(KEY_W_MEANING, meaning);
        cv.put(KEY_W_RATIO, ratio);
        cv.put(KEY_W_DISPLAY, display);
        return mDb.insert(WORDS_TABLE, null, cv);
    }


    /*all words*/
    public Cursor fetchWords() {
        return mDb.query(WORDS_TABLE, new String[]{
                KEY_W_ID,
                KEY_W_WORD,
                KEY_W_VARIANT,
                KEY_W_MEANING,
                KEY_W_RATIO,
                KEY_W_DISPLAY}, null, null, null, null, null);

    }

    /*only where display = 1*/
    public Cursor fetchWordsDis() {
        return mDb.query(WORDS_TABLE, new String[]{
                KEY_W_ID,
                KEY_W_WORD,
                KEY_W_VARIANT,
                KEY_W_MEANING,
                KEY_W_RATIO,
                KEY_W_DISPLAY}, KEY_W_DISPLAY +" = '1'", null, null, null, null);

    }



    public void trunFix() {
        mDb.delete(WORDS_TABLE, null, null);
    }


    public static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_FIXTURE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS fixture");
            onCreate(db);
        }

    }
}
