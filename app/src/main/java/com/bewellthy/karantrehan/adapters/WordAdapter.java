package com.bewellthy.karantrehan.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bewellthy.karantrehan.R;
import com.bewellthy.karantrehan.models.Word;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Karan Trehan on 04-03-2016.
 */
public class WordAdapter extends RecyclerView.Adapter<WordAdapter.ViewHolder> {
    Context mCon;
    private LayoutInflater inflater;

    private int lastPosition = -1;

    String imPref;

    List<Word> data = Collections.emptyList();

    public WordAdapter(Context context, List<Word> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mCon = parent.getContext();
        View view = inflater.inflate(R.layout.word_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Word current = data.get(position);
        imPref = mCon.getResources().getString(R.string.imPref);

        holder.tvWord.setText(current.word);
        holder.tvMeaning.setText(current.meaning);

        String imgPath = imPref+current.id+".png";

        Picasso.with(mCon)
                .load(imgPath)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(holder.ivImage);

        setAnimation(holder.cardView, position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mCon, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'word_row.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivImage)
        ImageView ivImage;
        @Bind(R.id.tvWord)
        TextView tvWord;
        @Bind(R.id.tvMeaning)
        TextView tvMeaning;
        @Bind(R.id.card_view)
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}